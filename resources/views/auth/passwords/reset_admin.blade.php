@extends('layouts.app_main')

@section('title', 'reset admin password')

@section('form')
<div id="div3" >
<form class="flex-container " method="POST" action="{{ route('admin.reset') }}">
            @csrf
            <input type="hidden" name="token" value="{{ $token }}">
            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required autofocus>
            @if ($errors->has('email'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                <input type="password" id="password" name="password"  >
                @if ($errors->has('password'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
         
                <label for="password-confirm">{{ __('Confirm Password') }}</label>
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                <input type="submit" value="Sing in">
        </form>
</div>
@endsection


