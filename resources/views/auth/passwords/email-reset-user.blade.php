@extends('layouts.app_main')

@section('title', 'request of reset admin password')

@section('form')
<div id="div3" >
<form class="flex-container " method="POST" action="{{ url('user_send_email') }}">
        @csrf
                <input type="email" id="email" name="email" value="{{ old('email') }}" >
                @if ($errors->has('email'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
                <input type="submit" value="Sing in">

        </form>
    </div>
@endsection