@extends('layouts.app_main')

@section('title', 'Welcome to admin panel entry')

@section('form')
<div id="div3" >
        <form class="flex-container " method="POST" action="{{ route('login_admin') }}">
        @csrf
                <input type="email" id="email" name="email" value="{{ old('email') }}" >
                @if ($errors->has('email'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
                <input type="password" id="password" name="password"  >
                @if ($errors->has('password'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
                <input type="submit" value="Sing in">
                <a  href="{{ route('admin.password.reset.email') }}">Forgot you password?</a>
        </form>
        
    </div>
@endsection

