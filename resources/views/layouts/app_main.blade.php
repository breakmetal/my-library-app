<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>
    <style>
        body{ background:#3c5a61; margin:0px;color:white}
        .flex-container{
            display: flex;
            flex-direction: column;
        }
        input{
            padding:10px;
            margin:5px;
            width:40%;
            background-color:#32454C;
            border-style: solid;
            border-color: #32454C;
            color:white;
            border-radius:5px;
            font-size:18px;
        }

        #div1{ background: #70c8c9;align-items: center;}
        #logo{margin-top:2%;}
        #div2{ align-items: center;}
        form{margin-top:2%;align-items: center;color:white}
        input[type="submit"]{background-color:#6DC9CA;color:white}
        a:hover {color:#6DC9CA}
    </style>
</head>
<body>
    <div class="flex-container">
    <div id="div1" class="flex-container">
        <img id="logo" src="{{ url('storage/images/library-app-logo.svg') }}" width=30%>
        <h1>@yield('title')</h1>
    </div>
    <div id="div2" class="flex-container">
        <img src="{{ url('storage/images/triangle.svg') }}"  width=8%>
    </div>
    <div id="div3" >
        @yield('form')
        
    </div>
    </div>
</body>
</html>