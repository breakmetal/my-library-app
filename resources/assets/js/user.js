require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//Vue.component('example-component', require('./components/ExampleComponent.vue'));
window.Vue = require('vue');
Vue.config.devtools = true
import Vuetify from 'vuetify'
import vuetify_css from 'vuetify/dist/vuetify.min.css'
import App from './components/user/index.vue'
import router from './routers/routers_user'
Vue.use(Vuetify)
Vue.use(vuetify_css)
export const bus=new Vue()
import infoUser from "./components/mixins/infoUser"
Vue.mixin(infoUser)
const app = new Vue({
    el: '#appAdmin',
    components: { App },
    template: '<app></app>',
    router,
});

