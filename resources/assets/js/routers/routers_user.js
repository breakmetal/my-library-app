import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

const router = new VueRouter({
    routes: [
        {path: '/', component: require('../components/user/index.vue')},
        {path: '/Books', component: require('../components/user/books.vue')},
        {path: '/Books_view/:id', component: require('../components/user/book_view.vue')},
        {path: '/Debts', component: require('../components/user/debts.vue')},
        {path: '/Loans', component: require('../components/user/loans.vue')},
        {path: '/Users_update/:id/:name/edit', component: require('../components/user/user_update.vue'), meta: {mode: 'edit'}},
        //LEIL LOWNDES como enamorar
        
       
    ]
})
export default router