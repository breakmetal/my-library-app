import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

const router = new VueRouter({
    routes: [
        {path: '/', component: require('../components/admin/welcome.vue')},
        {path: '/Admins', component: require('../components/admin/admins.vue')},
        {path: '/Admins_view/:id', component: require('../components/admin/admin_view.vue')},
        {path: '/Admins_create', component: require('../components/admin/admin_form.vue')},
        {path: '/Admins_update/:id/:name/edit', component: require('../components/admin/admin_update.vue'), meta: {mode: 'edit'}},
        {path: '/Authors', component: require('../components/admin/authors.vue')},
        {path: '/Authors_create', component: require('../components/admin/authors_form.vue')},
        {path: '/Authors_update/:id/:name/edit', component: require('../components/admin/authors_form.vue'), meta: {mode: 'edit'}},
        {path: '/Books', component: require('../components/admin/books.vue')},
        {path: '/Books_view/:id', component: require('../components/admin/book_view.vue')},
        {path: '/Books_create', component: require('../components/admin/books_form.vue')},
        {path: '/Books_update/:id/:name/edit', component: require('../components/admin/books_form.vue'), meta: {mode: 'edit'}},
        {path: '/Loans_create/:id', component: require('../components/admin/loans_form.vue')},
        {path: '/Editorials', component: require('../components/admin/editorials.vue')},
        {path: '/Editorials_create', component: require('../components/admin/editorials_form.vue')},
        {path: '/Editorials_update/:id/:name/edit', component: require('../components/admin/editorials_form.vue'), meta: {mode: 'edit'}},
        {path: '/Themes', component: require('../components/admin/themes.vue')},
        {path: '/Themes_create', component: require('../components/admin/themes_form.vue')},
        {path: '/Themes_update/:id/:name/edit', component: require('../components/admin/themes_form.vue'), meta: {mode: 'edit'}},
        {path: '/Users', component: require('../components/admin/users.vue')},
        {path: '/Users_create', component: require('../components/admin/users_form.vue')},
        {path: '/Users_update/:id/:name/edit', component: require('../components/admin/users_form.vue'), meta: {mode: 'edit'}},
        {path: '/Users_view/:id', component: require('../components/admin/users_view.vue')},
        
       
    ]
})
export default router