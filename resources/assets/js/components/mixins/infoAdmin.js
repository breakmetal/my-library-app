export default{
    data: () => ({
        userAdmin:{}
    }),
    created: function () {
        let vm=this
            axios.get(`admin/getInfo`)
            .then(function(response) {
                Vue.set(vm.$data,'userAdmin',response.data)
            })
    }
}