export default{
    data: () => ({
        user:{}
    }),
    /*mounted: function () {
        let vm=this
            axios.get(`user/getInfo`)
            .then(function(response) {
                Vue.set(vm.$data,'user',response.data)
            })
    },*/
    methods:{
        getInfoUser(){
            let vm=this
            axios.get(`user/getInfo`)
            .then(function(response) {
                Vue.set(vm.$data,'user',response.data)
            })
        },
        getNameUser(){
            return this.user.name
        }
    }
}