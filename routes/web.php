<?php

//use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Route;
use Illuminate\View\View;
//use Illuminate\Routing\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/
Route::get('/login_admin_panel',function () {
    return view('auth/login_admin');
})->name('login_index');
/* Those routes for admin options*/
Route::get('logout',function () {
    return view('auth/login_admin');
})->name('logout');
Route::post('login_admin','Auth\LoginController@login_admin')->name('login_admin');
Route::get('admin-password/reset',function(){
    return view('auth.passwords.email-reset-admin');
})->name('admin.password.reset.email');
Route::get('password/reset/{token}','Auth\AdminResetPasswordController@showResetForm')->name('admin.password.reset');
Route::post('admin_send_email','Auth\AdminForgotPasswordController@sendResetLinkEmail');
Route::post('admin/reset','Auth\AdminResetPasswordController@reset')->name('admin.reset');
/* Those routes for admin options*/
Route::prefix('admin')->middleware('auth:admin')->group(function () {
    Route::get('/', function () {
        return view('admin.welcomeLibrarian');
    });
    Route::resource('Admins','AdminController');
    Route::get('AdminsLogout','AdminController@logout');
    Route::resource('Authors', 'AuthorController');
    Route::get('Books/authors','BookController@authors');
    Route::get('Books/themes','BookController@themes');
    Route::get('Books/editorials','BookController@editorials');
    Route::get('Books/check/{id}','BookController@check');
    Route::resource('Books', 'BookController');
    Route::get('Loans/debtsUser/{id}','LoanController@debtsUser');
    Route::get('Loans/userLoads/{id}','LoanController@userLoads');
    Route::get('Loans/destroyDebt/{id}','LoanController@destroyDebt');
    Route::resource('Loans', 'LoanController');
    Route::resource('Editorials', 'EditorialController');
    Route::resource('Themes', 'ThemesController');
    Route::post('RegisterUser','Auth\RegisterController@create');
    //this is for login
    Route::get('/getInfo', function() {
        return Auth::user();
    });
    Route::get('logout','LoginController@logout');
    //this is for login
    Route::get('Users','Auth\UserController@listUsers');
    Route::delete('Users/{id}','Auth\UserController@destroy');
    Route::put('Users/{id}','Auth\UserController@update');
    Route::get('userShow/{id}','Auth\UserController@show');
});

Route::get('/login_user_panel',function () {
    return view('auth/login_user');
})->name('login_index_user');
Route::get('user-password/reset',function(){
    return view('auth.passwords.email-reset-user');
})->name('user.password.reset.email');
Route::post('user_send_email','Auth\UserForgotPasswordController@sendResetLinkEmail');
Route::post('login_user','Auth\LoginUserController@login_user')->name('login_user');
Route::get('user-password/reset/{token}','Auth\ResetPasswordController@showResetForm')->name('user.password.reset');
Route::post('user/reset','Auth\ResetPasswordController@reset')->name('user.reset');
Route::prefix('user')->middleware('auth:web')->group(function () {
    Route::get('/', function () {
        return view('user.welcomeLibrarian');
    });
    Route::resource('Users', 'Auth\UserController')->only([
        'show', 'update'
    ]);
    Route::resource('Books', 'BookController')->only([
        'index', 'show'
    ]);
    Route::resource('Loans', 'LoanController')->only([
        'index'
    ]);
    Route::get('Loans/userLoads/{id}','LoanController@userLoads');
    Route::get('Loans/debtsUser/{id}','LoanController@debtsUser');
    Route::get('UsersLogout','Auth\UserController@logout');
    Route::get('/getInfo', function() {
        return Auth::user();
    });
});
//
//Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
