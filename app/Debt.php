<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Debt extends Model
{
    public $incrementing=false;
    protected $fillable=['id','debt','user_id','created_at','updated_at'];

    public function user(){
        return $this->belongsTo('App\User');
    }
}
