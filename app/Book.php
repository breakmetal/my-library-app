<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\myTraits\FilterSearch;
class Book extends Model
{
    use FilterSearch;
    protected $fillable = [ 
        'id', 
        'editorial_id', 
        'author_id', 
        'title',
        'publication',
        'ubication',
        'available',
        'created_at',
        'updated_at'
    ];
    protected $filter = [ 
        'id', 
        'editorial', 
        'author', 
        'title',
        'publication',
        'ubication',
        'available',
        'themes'
    ];
    protected $relations = ['editorial','author','themes'];
    public function themes()
    {
        return $this->belongsToMany('App\Theme');
    }
    public function author()
    {
        return $this->belongsTo('App\Author');
    }
    public function editorial()
    {
        return $this->belongsTo('App\Editorial');
    }
    public function loan()
    {
        return $this->hasOne('App\Loan');
    }
}
