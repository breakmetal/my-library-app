<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\myTraits\FilterSearch;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\MailResetPassword;

class Admin extends Authenticatable
{
    use Notifiable;
    use FilterSearch;

    protected $guard='admin';
    protected $fillable = [
        'id','name','email','role','phone','password','created_at','updated_at'
    ];
    protected $filter=['id','name', 'email','phone','created_at','updated_at'];

    protected $hidden = [
        'password', 'remember_token',
    ];
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MailResetPassword($token));
    }
}
