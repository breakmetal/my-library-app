<?php

namespace App\Http\Controllers;
use App\Author;
use Illuminate\Http\Request;
use App\Theme;
use App\Editorial;
use App\Book;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books=Book::filterSearch();
        $books->each(function($books){
            $books->themes;
            $books->author;
            $books->editorial;
        });
        return response()->json($books); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $book=new Book($request->all());
        $book->save();
        $book->themes()->sync($request->themes_id);
        return 'the book '.$request->title.' was registered successfully';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $book=Book::find($id);
        $book->themes;
        $book->author;
        $book->editorial;
        return response()->json($book);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $book=Book::find($id);
        $book->available='yes';
        $book->save();
        return 'ok';
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $book=Book::find($id);
        $book->title=$request->title;
        $book->editorial_id=$request->editorial_id;
        $book->author_id=$request->author_id;
        $book->publication=$request->publication;
        $book->save();
        $book->themes()->detach();
        $book->themes()->sync($request->themes_id);
        return 'the register was updated';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book=Book::find($id);
        //$book->themes()->detach();
        $book->delete();
        return 'the register was deleted';
    }
    public function authors(){
        $authors=Author::relationSelect();
        return response()->json($authors);
    }
    public function themes(){
        $themes=Theme::relationSelect();
        return response()->json($themes);
    }
    public function editorials(){
        $editorials=Editorial::relationSelect();
        return response()->json($editorials);
    }
    //this method is included to avoid do joins unnecessary, inasmuch is not required the all data related with author
    public function check($id){
        $book=Book::find($id);
        if($book==null){
            $book=new Book();
            $book->title='not found';
        }elseif( $book->available==='no' ){
            $book=new Book();
            $book->title='is not available';
        }
        return $book;
    } 
}
