<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use App\Loan;
use Illuminate\Http\Request;
use App\User;
use App\Book;
use App\Debt;
use Prophecy\Exception\Doubler\ReturnByReferenceException;
use Illuminate\Support\Facades\Auth;

class LoanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $loan=Loan::scopeFilterSearch();
        return $loan;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $debts=Debt::where('user_id',$request->user_id)->count();
        if($debts>0){
            return 'please cancel your debts';
        }
        elseif($request->book!='not found'){
            $loan=new Loan($request->all());
            $book=Book::find($request->book_id);
            $book->available='no';
            $book->save();
            $loan->id=$request->book_id;
            $loan->delivery_date=Carbon::now();
            $loan->delivery_date->addDays(5);
            $loan->save();
            return 'the regiter is successfull'; 
        }else{
            return 'please, check the book_id';
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Borrow  $borrow
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $borrow=Loan::where('book_id',$id)->get();
        $borrow->each(function($borrow){
            $borrow->user;
        });
        return response()->json($borrow);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Borrow  $borrow
     * @return \Illuminate\Http\Response
     */
    public function edit(Borrow $borrow)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Borrow  $borrow
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Borrow $borrow)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Borrow  $borrow
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $loan=Loan::find($id);
        $now= Carbon::now();
        if($now<$loan->delivery_date){
            $loan->delete();
            return 'you are early';
        }else{
            $days=$now->diffInDays($loan->delivery_date);
            $debt=new Debt();
            $debt->user_id=$loan->user_id;
            $debt->id=$loan->id;
            $debt->debt=$days*100;
            $debt->save();
            $loan->delete();
            return 'you have debt';
        }
    }
    public function userLoads($id){
        $id_user=$id;
        if(Auth::guard('web')->check()){    
            $id_user=Auth::user()->id;

        }
        $userLoads=Loan::where('user_id',$id_user)->get();
        $userLoads->each(function ($userLoads){
            $userLoads->book;
        });
        return  response()->json($userLoads);
    }
    public function debtsUser($id){
        $debt;
        $id_user=$id;
        if(Auth::guard('web')->check()){    
            $id_user=Auth::user()->id;
        }
        $debt=Debt::where('user_id',$id_user)->get();
        return response()->json($debt);
    }
    public function destroyDebt($id){
        $debt=Debt::find($id);
        $debt->delete();
        return 'Debt settled';
    }
}
