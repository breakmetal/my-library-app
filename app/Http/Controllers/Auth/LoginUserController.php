<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LoginUserController extends Controller
{
     /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    //use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'user';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function login(Request $request){
       
        $credentials=$request->validate([
            'email' => 'email|required',
            'password' => 'required|string'
        ]);
        $user_data = array(
           'email' => $request->email,
           'password' => $request->password
        );
        if(Auth::attempt($user_data)){
            return redirect('user');
        }else{
            return back()->withErrors(['email' => 'this credentials do not match with our registers']);
        }
       
    }

    public function login_user(Request $request){
        //36 35
        $credentials=$request->validate([
            'email' => 'email|required',
            'password' => 'required|string'
        ]);
        $user_data = array(
           'email' => $request->email,
           'password' => $request->password
        );
        if(Auth::guard('web')->attempt($user_data)){
            return redirect('user');
        }else{
            return back()->withErrors(['email' => 'this credentials do not match with our registers'])->withInput();
        }
       
    }
    
    protected function guard()
    {
    return Auth::guard('web');
    }
}
