<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller{
    protected function update(Request $request, $id)
    {
        $user=User::find($id);
        $user->name=$request->name;
        $user->phone=$request->phone;
        $user->save();
        return 'the register was updated';
    }
    protected function listUsers(){
        $user=User::filterSearch();
        return response()->json($user); 
    }
    protected function destroy($id){
        $user=User::find($id);
        $user->delete();
        return 'the register was deleted';
    }
    protected function show($id)
    {
        $user=User::find($id);
        return $user;
    }
    protected function showChangePasswordForm(){
        return view('auth.changepassword');
    }
    public function login(Request $request){
        $credentials=$request->validate([
            'email' => 'email|required',
            'password' => 'required|string'
        ]);
        return $credentials;
        $user_data = array(
           'email' => $request->email,
           'password' => $request->password
        );
        if(Auth::attempt($user_data)){
            return 'success full';
        }
       return back()->withErrors(['email' => 'this credentials do not match with our registers']);
    }
    public function logout(){
        Auth::logout();
    }
}