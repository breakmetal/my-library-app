<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

//use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    //use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    /*public function login(Request $request){
       
        $credentials=$request->validate([
            'email' => 'email|required',
            'password' => 'required|string'
        ]);
        $user_data = array(
           'email' => $request->email,
           'password' => $request->password
        );
        if(Auth::attempt($user_data)){
            return redirect('admin');
        }else{
            return back()->withErrors(['email' => 'this credentials do not match with our registers']);
        }
       
    }*/

    public function login_admin(Request $request){
        $credentials=$request->validate([
            'email' => 'email|required',
            'password' => 'required|string'
        ]);
        $user_data = array(
           'email' => $request->email,
           'password' => $request->password
        );
        if(Auth::guard('admin')->attempt($user_data)){
            return redirect('admin');
        }else{
            return back()->withErrors(['email' => 'this credentials do not match with our registers'])->withInput();
        }
       
    }
    public function logout(){
        Auth::logout();
        return redirect("/login_admin_panel");
    }
    protected function guard()
    {
    return Auth::guard('admin');
    }
    
}
