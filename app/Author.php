<?php

namespace App;
use App\myTraits\FilterSearch;
use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    use FilterSearch;
    public $timestamps = false;
    protected $fillable=['id', 'name' ];    
    public function books()
    {
        return $this->hasMany('App\Book');
    }

    protected $filter = ['id','name'];
}
