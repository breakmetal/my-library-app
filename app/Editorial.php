<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\myTraits\FilterSearch;
class Editorial extends Model
{
    use FilterSearch;
    public $timestamps = false;
    protected $fillable=['id', 'name' ]; 
    public function books()
    {
        return $this->hasMany('App\Book');
    }
    protected $filter = ['id','name'];
}
