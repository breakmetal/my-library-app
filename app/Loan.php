<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\myTraits\FilterSearch;
class Loan extends Model
{
    use FilterSearch;
    public $incrementing=false;
    protected $fillable=['id','active','book_id','user_id','delivery_date','created_at','updated_at']; 
    public function book()
    {
        return $this->belongsTo('App\book');
    }
    public function user()
    {
        return $this->belongsTo('App\user');
    }
    protected $filter = ['id','active','book_id','user_id','delivery_date','created_at','updated_at'];
}
