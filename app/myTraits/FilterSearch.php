<?php

namespace App\myTraits;
use Validator;
trait FilterSearch{

    protected $operators = [ '=','<','>'];
    

    public function scopeFilterSearch( $query ){
        $request = request();
        $v = Validator::make($request->all(), [
            'column' => 'required|in:'.implode(',', $this->filter),
            'direction' => 'required|in:asc,desc',
            //'operator' => 'required|in:'.implode(',', $this->operators),
            'search_column' => 'required|in:'.implode(',', $this->filter),
            'search_query' => 'max:30',
        ]);
        if($v->fails()){
            return $v->errors();
        }
        return $query->orderBy($request->column, $request->direction)
        ->where(function($query) use ($request) {
            if(!empty($request->search_query)){
                if(in_array($request->search_column,$this->relations)){
                    return $query->whereHas($request->search_column,function($query) use ($request) {
                        $query->where('name','like', '%'.$request->search_query.'%');
                    });
                }
                elseif($request->operator==="=" && !is_numeric($request->search_query)  ){
                    return $query->where($request->search_column,'like', '%'.$request->search_query.'%');
                }elseif($request->operator==="<" || $request->operator===">"){
                    return $query->where($request->search_column,$request->operator, $request->search_query);
                }elseif($request->operator==="="){
                    return $query->where($request->search_column,$request->operator, $request->search_query);
                }
            }
        })->paginate($request->per_page);
    }

    public function scopeRelationSelect( $query ){
        $request = request();
        return $query->select('name','id')->where('name','like','%'.$request->name.'%')->get();
    }

}