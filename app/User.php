<?php

namespace App;
use App\myTraits\FilterSearch;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\MailResetPasswordUser;

class User extends Authenticatable
{
    use Notifiable;
    use FilterSearch;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','name','email','phone','password','created_at','updated_at'
    ];
    protected $filter=['id','name','email','phone','created_at','updated_at'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function loan()
    {
        return $this->hasMany('App\Loan');
    }
    public function debts()
    {
        return $this->hasMany('App\Debt');
    }
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MailResetPasswordUser($token));
    }
}
