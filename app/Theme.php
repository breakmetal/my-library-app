<?php

namespace App;
use App\myTraits\FilterSearch;
use Illuminate\Database\Eloquent\Model;

class Theme extends Model
{
    use FilterSearch;
    public $timestamps = false;
    protected $fillable=['id', 'name' ];    
    protected $filter = ['id','name'];
    public function books()
    {
        return $this->belongsToMany('App\Book');
    }
}
