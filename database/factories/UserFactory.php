<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'id' => $faker->unique()->randomNumber,
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'phone' => $faker->phoneNumber,
        'password' => bcrypt('123456'), // secret
        'remember_token' => str_random(10),
    ];
});
$factory->define(App\Author::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
    ];
});
$factory->define(App\Editorial::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
    ];
});
$factory->define(App\Book::class, function (Faker $faker) {
    $editorials=App\Editorial::pluck('id')->toArray();
    $autors=App\Author::pluck('id')->toArray();
    $ubication=['floor1','floor2','floor3','floor4'];
    return [
        'editorial_id' => $faker->randomElement($editorials),
        'author_id' => $faker->randomElement($autors),
        'title' => $faker->sentence($nbWords = 4, $variableNbWords = true),
        'publication' => $faker->date,
        'ubication' => $faker->randomElement($ubication),
        'available' => 'yes'
    ];
});

