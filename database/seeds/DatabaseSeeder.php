<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\User;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    
    public function run()
    {
        $themes=['action','romance','calculus','physics','sciencie','sciencie fiction','fantasy','languge','chemestry','terror','history'];
        factory(App\User::class,20)->create();
        factory(App\Author::class,20)->create();
        factory(App\Editorial::class,20)->create();
        for($i=0;$i<9;$i++){
            DB::table('themes')->insert([
                'name' => $themes[$i]
            ]);
        }
        factory(App\Book::class,40)->create()->each(function($book){
            $book->themes()->sync(
                App\Theme::all()->random(3)
            );
        });
    }
}
